import java.util.Arrays;

/**
 * Предусмотреть функциональный интерфейс
 * <p>
 * interface ByCondition {
 * boolean isOk(int number);
 * }
 * <p>
 * Реализовать в классе Sequence метод:
 * <p>
 * public static int[] filter(int[] array, ByCondition condition) {
 * ...
 * }
 * <p>
 * Данный метод возвращает массив, который содержит элементы, удовлетворяющиие логическому выражению в condition.
 * <p>
 * В main в качестве condition подставить:
 * <p>
 * - проверку на четность элемента
 * - проверку, является ли сумма цифр элемента четным числом.
 */

public class Main {
    static ByCondition condition1 = number -> number % 2 == 0;
    static ByCondition condition2 = Main::isSumDigitsEven;

    public static void main(String[] args) {
        int[] arr = new int[]{25, 78, 4, 53, 4235, 23, 24};
        int[] rezultArr1 = Sequence.filter(arr, condition1);
        int[] rezultArr2 = Sequence.filter(arr, condition2);
        System.out.println(Arrays.toString(rezultArr1));
        System.out.println(Arrays.toString(rezultArr2));

    }
    public static boolean isSumDigitsEven(int value) {
        int sumDigits = 0;

        while (value != 0) {
            sumDigits += value % 10;
            value /= 10;
        }
        return sumDigits % 2 == 0;
    }

}

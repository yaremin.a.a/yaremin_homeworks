import java.util.Arrays;

public class Sequence {

    public static int[] filter(int[] array, ByCondition condition) {
        int[] newArray = new int[array.length];
        int count = 0;
        for (int numberFromArray : array) {

            if (condition.isOk(numberFromArray)) {
                newArray[count] = numberFromArray;
                count++;
            }

        }
        newArray = Arrays.copyOf(newArray, count);

        return newArray;

    }

}

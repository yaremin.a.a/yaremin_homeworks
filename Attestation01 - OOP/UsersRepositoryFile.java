import java.util.List;

public interface UsersRepositoryFile {
    List<User> findAll();
    List<User> findById(List<User> userList, int id);
    void update (User user);
}

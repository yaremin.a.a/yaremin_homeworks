import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class UsersRepositoryFileImpl implements UsersRepositoryFile {
    List<User> userList;
    private String fileName;

    public UsersRepositoryFileImpl(String fileName) {

        this.fileName = fileName;
    }


    @Override
    public List<User> findAll() {
        List<User> users = new ArrayList<>();
        // объявили переменные для доступа
        Reader reader = null;
        BufferedReader bufferedReader = null;
        try {
            // создали читалку на основе файла
            reader = new FileReader(fileName);
            // создали буферизированную читалку
            bufferedReader = new BufferedReader(reader);
            // прочитали строку
            String line = bufferedReader.readLine();
            // пока к нам не пришла "нулевая строка"
            while (line != null && !line.isEmpty()) {
                // разбиваем ее по |
                String[] parts = line.split("\\|");
                int id = Integer.parseInt(parts[0]);
                // берем имя
                String name = parts[1];
                // берем возраст
                int age = Integer.parseInt(parts[2]);
                // берем статус о работе
                boolean isWorker = Boolean.parseBoolean(parts[3]);
                // создаем нового человека
                User newUser = new User(id, name, age, isWorker);
                // добавляем его в список
                users.add(newUser);
                // считываем новую строку
                line = bufferedReader.readLine();
            }

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        } finally {
            // этот блок выполнится точно
            if (reader != null) {
                try {
                    // пытаемся закрыть ресурсы
                    reader.close();
                } catch (IOException ignore) {
                }
            }
            if (bufferedReader != null) {
                try {
                    // пытаемся закрыть ресурсы
                    bufferedReader.close();
                } catch (IOException ignore) {
                }
            }
        }

        return users;


    }

    @Override
    public List<User> findById(List<User> userList, int id) {
        this.userList = userList;
        return this.userList.stream()
                .filter(user -> user.getId() == id)
                .collect(Collectors.toList());
    }

    @Override
    public void update(User user) {
        List<User> users = new ArrayList<>();
        // объявили переменные для доступа
        Reader reader = null;
        BufferedReader bufferedReader = null;
        try {
            // создали читалку на основе файла
            reader = new FileReader(fileName);
            // создали буферизированную читалку
            bufferedReader = new BufferedReader(reader);
            // прочитали строку
            String line = bufferedReader.readLine();
            // пока к нам не пришла "нулевая строка"
            while (line != null && !line.isEmpty()) {
                // разбиваем ее по |
                String[] parts = line.split("\\|");
                int id = Integer.parseInt(parts[0]);
                if (user.getId() == id) {
                    users.add(user);
                } else {
                    // берем имя
                    String name = parts[1];
                    // берем возраст
                    int age = Integer.parseInt(parts[2]);
                    // берем статус о работе
                    boolean isWorker = Boolean.parseBoolean(parts[3]);
                    // создаем нового человека
                    User newUser = new User(id, name, age, isWorker);
                    // добавляем его в список
                    users.add(newUser);
                }

                // считываем новую строку
                line = bufferedReader.readLine();
            }

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        } finally {
            // этот блок выполнится точно
            if (reader != null) {
                try {
                    // пытаемся закрыть ресурсы
                    reader.close();
                } catch (IOException ignore) {
                }
            }
            if (bufferedReader != null) {
                try {
                    // пытаемся закрыть ресурсы
                    bufferedReader.close();
                } catch (IOException ignore) {
                }
            }
        }
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(fileName))) {
            users.stream().
                    map(u -> u.getId() + "|" + u.getName() + "|" + u.getAge() + "|" + u.isWorker() + "\n")
                    .forEach(u -> {
                        try {
                            writer.write(u);
                        } catch (IOException ignore) {

                        }
                    });
        } catch (IOException ignore) {

        }
    }

}

import java.util.List;
import java.util.Scanner;

/**
 * Реализовать в классе UsersRepositoryFileImpl методы:
 * User findById(int id);
 * update(User user);
 * <p>
 * Принцип работы методов:
 * Пусть в файле есть запись:
 * 1|Игорь|33|true
 * Где первое значение - гарантированно уникальный ID пользователя (целое число).
 * Тогда findById(1) вернет объект user с данными указанной строки.
 * <p>
 * Далее, для этого объекта можно выполнить следующий код:
 * user.setName("Марсель");
 * user.setAge(27);
 * и выполнить update(user);
 * При этом в файле строка будет заменена на 1|Марсель|27|true.
 * Таким образом, метод находит в файле пользователя с id user-а и заменяет его значения.
 */
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        UsersRepositoryFile usersRepositoryFile = new UsersRepositoryFileImpl("Users.txt");

        List<User> users = usersRepositoryFile.findAll();
        int idForSearch;
        String newName;
        int newAge;
        for (User user : users) {
            System.out.println(user.getId() + " " + user.getName() + " " + user.getAge() + " " + user.isWorker());
        }
        System.out.println("Введите id полизователся для обновления");
        idForSearch = scanner.nextInt();
        System.out.println("Введите новое имя для пользователся");
        newName = scanner.next();
        System.out.println("Введите новый возраст для пользователя");
        newAge = scanner.nextInt();
        List<User> usersResults = usersRepositoryFile.findById(users, idForSearch);

        for (User user : usersResults) {
            user.setName(newName);
            user.setAge(newAge);
            User userForUpdate = new User(user.getId(), user.getName(), user.getAge(), user.isWorker());
            usersRepositoryFile.update(userForUpdate);
        }

    }
}

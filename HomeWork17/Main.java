/*
 * На вход подается строка с текстом. Необходимо посчитать, сколько встречается раз каждое слово в этой строке.
 * <p>
 * Вывести:
 * Слово - количество раз
 * <p>
 * Использовать Map, string.split(" ") - для деления текста по словам. Слово - символы, ограниченные пробелами справа и слева.
 */

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Map<String, Integer> map = new HashMap<>();
        System.out.println("Введите текст");
        String str = scanner.nextLine();
        String[] words = str.split(" ");

        for (String word : words) {
            if (map.containsKey(word)) {
                int count = map.get(word);
                map.put(word, count + 1);
            } else {
                map.put(word, 1);
            }
        }
        Set<Map.Entry<String, Integer>> entries = map.entrySet();
        for (Map.Entry<String, Integer> entry : entries) {
            String end;
            if (entry.getValue() != 2 & entry.getValue() != 3 & entry.getValue() != 4) {
                end = "раз";
            } else {
                end = "раза";
            }
            System.out.println("Слово - " + entry.getKey() + " повторяется - " + entry.getValue() + " " + end);
        }
    }
}

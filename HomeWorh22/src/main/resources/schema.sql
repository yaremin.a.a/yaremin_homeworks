
-- Создаём таблице с товаром (книги)
create table product
(
    id serial primary key,  -- первичный ключ
    description varchar (100),  -- название книги
    cost integer check(cost>0),  -- стоимость одной книги
    amount integer check ( amount >=0)  -- количество книг в магазине
);
-- вставляем в таблицу новые строки
insert into product(description, cost, amount)
values ('Фантастика', 100, 72);
insert into product(description, cost, amount)
values ('Детектив', 50, 85);
insert into product(description, cost, amount)
values ('Сказака', 19, 45);
insert into product(description, cost, amount)
values ('Русская класика', 250, 5);

select * from product;  -- Запрос на выборку всех книг

update product set description = 'Русская классика' where id = 4;  -- обновляем строку
-- Создаём таблицу с клиентами
create table client
(
    id serial primary key,  -- первичный ключ
    first_name varchar(20),  -- Имя
    last_name  varchar(20)  -- Фамилия
);
-- Добавляем строки в таблицу
insert into client (first_name, last_name)
values ('Владимир', 'Путин');
insert into client (first_name, last_name)
values ('Ангела', 'Меркель');
insert into client (first_name, last_name)
values ('Дональд', 'Трамп');
insert into client (first_name, last_name)
values ('Мануэль', 'Макрон');
-- Запрос на получение количества клиентов
select count(*) from client;
-- Создаём таблицу со списком заказов
create table list_order
(
    id serial primary key, -- первичный ключ
    id_product integer,  -- внешний ключ
    id_client integer,  -- внешний ключ
    date date,  -- дата заказа
    amount_of_product integer,  -- количество книг в заказе
    foreign key (id_product) references product(id),  -- ссылка на первичный ключ таблицы product
    foreign key (id_client) references client(id)  --ссылка на первичный ключ таблицы client
);
-- Добавляем строки в таблицу
insert into list_order (id_product, id_client, date, amount_of_product)
values (1, 1, current_date, 5);
insert into list_order (id_product, id_client, date, amount_of_product)
values (1, 2, current_date, 1);
insert into list_order (id_product, id_client, date, amount_of_product)
values (1, 4, (current_date-1), 2);
insert into list_order (id_product, id_client, date, amount_of_product)
values (1, 1, (current_date-5), 5);
insert into list_order (id_product, id_client, date, amount_of_product)
values (4, 1, (current_date+1), 2);
insert into list_order (id_product, id_client, date, amount_of_product)
values (3, 3, current_date, 40);
insert into list_order (id_product, id_client, date, amount_of_product)
values (2, 2, current_date, 1);
-- Запрос на выборку данных с сортировкой по дате заказа
select * from list_order order by date;
-- Запрос на выборку данных о заказе клиента под номером 1 с сортировкой по дате
select * from list_order where id_client = 1  order by date;
-- Запрос на получение фамилий, которые заказывали Русскую классику
select last_name from client where client.id in (select id_client from list_order where id_product = 4);
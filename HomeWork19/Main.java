import java.util.List;
/*
Реализовать методы UsersRepository
 */
public class Main {
    public static void main(String[] args) {
        UsersRepository usersRepository = new UsersRepositoryFileImpl("users.txt");

        User user1 = new User("Кирилл", 30, false);
        usersRepository.save(user1);

        List<User> users = usersRepository.findAll();

        for (User user : users) {
            System.out.println(user.getAge() + " " + user.getName() + " " + user.isWorker());
        }

        List<User> ageUsers = usersRepository.findByAge(25);
        for (User user : ageUsers) {
            System.out.println(user.getAge() + " " + user.getName() + " " + user.isWorker());
        }
        List<User> userIsWorker = usersRepository.findByIsWorkerIsTrue();
        for (User user : userIsWorker) {
            System.out.println(user.getAge() + " " + user.getName() + " " + user.isWorker());
        }
    }

}

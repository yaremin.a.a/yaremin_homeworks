import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.util.List;

public class ProductsRepositoryImpl implements ProductsRepository {
    // language=SQL
    private static final String SQL_INSERT = "INSERT INTO product(description, cost, amount) values (?, ?, ?)";

    // language=SQL
    public static final String SQL_SELECT_ALL = "SELECT * from product order by id";

    // language=SQL
    public static final String SQL_SELECT_BY_PRICE = "SELECT * from product where cost=(?)";

    // language=SQL
    public static final String SQL_SELECT_BOrdersCount = """
            SELECT *
            FROM product p
            WHERE (SELECT COUNT(*) FROM list_order lo WHERE p.id = lo.id_product)=(?)""";

    public ProductsRepositoryImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    private final JdbcTemplate jdbcTemplate;
    private static final RowMapper<Product> productRowMapperaper = (row, rowNumber) -> {
        int id = row.getInt("id");
        String description = row.getString("description");
        int cost = row.getInt("cost");
        int amount = row.getInt("amount");
        return new Product(id, description, cost, amount);
    };

    @Override
    public List<Product> findAll() {

        return jdbcTemplate.query(SQL_SELECT_ALL, productRowMapperaper);
    }

    @Override
    public List<Product> findAllByPrice(int price) {

        return jdbcTemplate.query(SQL_SELECT_BY_PRICE, productRowMapperaper, price);
    }

    @Override
    public List<Product> findAllByOrdersCount(int ordersCount) {

        return jdbcTemplate.query(SQL_SELECT_BOrdersCount, productRowMapperaper, ordersCount);
    }

    @Override
    public void save(Product product) {
        jdbcTemplate.update(SQL_INSERT, product.getDescription(), product.getCost(), product.getAmount());
    }
}

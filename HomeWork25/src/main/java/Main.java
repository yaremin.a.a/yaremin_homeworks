/*
  Реализовать ProductsRepository

  - List<Product> findAll();
  - List<Product> findAllByPrice(double price);
  * List<Product> findAllByOrdersCount(int ordersCount); - найти все товары по количеству заказов, в которых участвуют
 */

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;
import java.io.OutputStream;

public class Main {
    public static void main(String[] args) {

        DataSource dataSource = new DriverManagerDataSource("jdbc:postgresql://localhost:5432/pcs_2",
                "postgres", "qwerty007");
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        ProductsRepository productsRepository = new ProductsRepositoryImpl(dataSource);

        Product product = Product.builder()
                .description("Биография")
                .cost(78)
                .amount(90)
                .build();

        System.out.println(productsRepository.findAll());
        System.out.println(productsRepository.findAllByPrice(100));
        System.out.println(productsRepository.findAllByOrdersCount(1));

    }
}

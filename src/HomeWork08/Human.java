package HomeWork08;

public class Human {
    private double weight; // вес

    private String name; // имя

    public double getWeight() {
        return this.weight;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setWeight(double weight) {
        if (weight < 0 || weight > 200) {
            weight = 0;
        }
        this.weight = weight;
    }
}

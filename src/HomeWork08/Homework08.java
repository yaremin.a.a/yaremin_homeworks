package HomeWork08;

import java.util.Scanner;

public class Homework08 {
    // процедура сортировки массива объектов
    static void sortListHumans(Human[] listHumans) {
        // по умолчанию принимаем, что первый элемент массива с самым маленьким весом
        for (int i = 0; i < listHumans.length; i++) {
            // переменная для хранения веса
            Human MinWeight = listHumans[i];
            // перемменная для хранения индекса
            int index = 0;
            // проходим по остатку массива объектов
            for (int j = i + 1; j < listHumans.length; j++) {
                // если находим вес меньше записанного ранее, то присваивем переменным веса и индекса новое значение
                if (listHumans[j].getWeight() < MinWeight.getWeight()) {
                    MinWeight = listHumans[j];
                    index = j;
                }
            }
            //меняем местами объекты в массиве
            Human tempHuman = listHumans[i];
            listHumans[index] = tempHuman;
            listHumans[i] = MinWeight;
        }
    }


    public static void main(String[] args) {
        // объявляем массив объектов с нужным размером
        Human[] listHumans = new Human[10];
        // объявляем переменную для считывания с консоли
        Scanner scanner = new Scanner(System.in);
        // запускаем цикл добавления объектов в массив
        for (int i = 0; i < listHumans.length; i++) {
            // переменная для хранения имени
            String name;
            // переменная для хранения веса
            double weight;
            // переменная для хранения порядкового номера
            int count = i + 1;
            System.out.print("Введите имя №" + count + ": ");
            name = scanner.next();
            System.out.print("Введите вес для " + name + ": ");
            weight = scanner.nextDouble();
            // После ввода параметров объекта, добавляем его в массив
            listHumans[i] = new Human();
            listHumans[i].setName(name);
            listHumans[i].setWeight(weight);
        }
        // выполняем процедуру сортировки
        sortListHumans(listHumans);
        // Выводим на экран отсортированный массив объектов
        for (int i = 0; i < listHumans.length; i++) {
            System.out.println(listHumans[i].getName() + " " + listHumans[i].getWeight());
        }
    }
}

package HomeWork09;

/**
 * Сделать класс Figure, у данного класса есть два поля - x и y координаты.
 * <p>
 * Классы Ellipse и Rectangle должны быть потомками класса Figure.
 * <p>
 * Класс Square - потомок класса Rectangle, Circle - потомок класса Ellipse.
 * <p>
 * В классе Figure предусмотреть метод getPerimeter(), который возвращает 0. Во всех остальных классах он должен
 * возвращать корректное значение.
 */
public class Main {
    public static void main(String[] args) {

        Figure figure = new Figure(15, 10);
        // создаем массив объектов
        Figure[] figures = new Figure[4];
        figures[0] = new Rectangle(0, 0, 15, 20);
        figures[1] = new Ellipse(0, 0, 10, 15);
        figures[2] = new Square(0, 0, 12);
        figures[3] = new Circle(0, 0, 10);
        // Выводим расчёт периметра по фигурам
        System.out.println("Периметр фигуры - " + figure.getPerimeter());
        System.out.println("Периметр прямоугольника - " + figures[0].getPerimeter());
        System.out.println("Периметр эллипса - " + figures[1].getPerimeter());
        System.out.println("Периметр квадрата - " + figures[2].getPerimeter());
        System.out.println("Периметр круга - " + figures[3].getPerimeter());
    }
}

package HomeWork09;

/**
 * Класс фигура
 */
public class Figure {
    private double x;
    private double y;

    /**
     * фигура
     *
     * @param x координата по х
     * @param y координата по у
     */
    public Figure(double x, double y) {
        this.x = x;
        this.y = y;
    }

    /**
     * метод расчёта периметра фигуры
     *
     * @return возвращет периметр
     */
    public double getPerimeter() {
        return 0;
    }
}

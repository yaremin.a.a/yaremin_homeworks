package HomeWork09;

/**
 * Эллипс - наследник Фигуры
 */
public class Ellipse extends Figure {
    private double a; // полуось а
    private double b; // полуось b

    public Ellipse(double x, double y, double a, double b) {
        super(x, y); // вызов конструктора предка
        this.a = a;
        this.b = b;
    }

    /**
     * Метод расчёта периметра эллипса P = 4(PI*a*b+(a-b)^2)/(a+b)
     *
     * @return возвращает периметр
     */
    public double getPerimeter() {
        return 4 * (Math.PI * this.a * b + (Math.pow((this.b - this.a), 2.0))) /
                (this.b + this.a);
    }
}

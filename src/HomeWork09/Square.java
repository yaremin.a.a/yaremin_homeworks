package HomeWork09;

/**
 * Квадрат - наследник Прямоугольника
 */
public class Square extends Rectangle {
    public Square(double x, double y, double a) {
        super(x, y, a, a); // вызов конструктора предка
    }
}

package HomeWork10;

public class Circle extends Ellipse implements MoveFigure {
    private double a;
    public Circle(double x, double y, double a) {
        super(x, y, a, a); // вызов конструктора предка
        System.out.println("Круг создал по слудующим координатам: х - "
                + this.x + " у - "+ this.y);
    }

    @Override
    public void moving(double newX, double newY) {

        this.x =  newX;
        this.y =  newY;
        System.out.println("Переместил кгруг на новые координаты: х - "+ this.x + " у - " + this.y);
    }

}

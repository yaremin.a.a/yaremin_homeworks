package HomeWork10;

/**
 * Класс фигура
 */
public abstract class Figure {
    protected double x;
    protected double y;
    protected MoveFigure moveFigure;
    /**
     * фигура
     *
     * @param x координата по х
     * @param y координата по у
     */
    public Figure(double x, double y) {
        this.x = x;
        this.y = y;
    }


    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    /**
     * метод расчёта периметра фигуры
     *
     * @return возвращет периметр
     */
    public abstract double getPerimeter();
}

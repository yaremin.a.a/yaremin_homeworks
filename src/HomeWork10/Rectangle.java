package HomeWork10;

/**
 * класс Прямоугольник - наследник Фигуры
 */
public class Rectangle extends Figure {
    private double a;
    private double b;

    /**
     * Прямоугольк
     *
     * @param x координата по х
     * @param y координата по у
     * @param a сторона а
     * @param b сторона b
     */

    public Rectangle(double x, double y, double a, double b) {
        super(x, y); // вызов конструктора предка
        this.a = a;
        this.b = b;
    }

    /**
     * Метод расчёта периметра прямоугольника
     *
     * @return возвращает периметр
     */
    public double getPerimeter() {
        return this.a * 2 + this.b * 2;
    }
}

package HomeWork10;

/**
 * Сделать класс Figure из задания 09 абстрактным.
 * <p>
 * Определить интерфейс, который позволит перемещать фигуру на заданные координаты.
 * <p>
 * Данный интерфейс должны реализовать только классы Circle и Square.
 * <p>
 * В Main создать массив "перемещаемых" фигур и сдвинуть все их в одну конкретную точку.
 */
public class Main {
    public static void main(String[] args) {

        MoveFigure[] moveFigure = new MoveFigure[2];
        moveFigure[0] = new Circle(15, 10, 5);
        moveFigure[1] = new Square(10, 20, 80);
        for (int i = 0; i < moveFigure.length; i++) {
            moveFigure[i].moving(7, 5);
        }

    }
}

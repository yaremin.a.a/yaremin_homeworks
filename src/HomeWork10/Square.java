package HomeWork10;

/**
 * Квадрат - наследник Прямоугольника
 */
public class Square extends Rectangle implements MoveFigure {
    private double a;

    public Square(double x, double y, double a) {
        super(x, y, a, a); // вызов конструктора предка
        System.out.println("Квадрат создал по следующим координатам: х - " + this.x + " y - " + this.y);
    }

    @Override
    public void moving(double newX, double newY) {
        this.x = newX;
        this.y = newY;
        System.out.println("Переместил квадрат на новые координаты: х - " + this.x + " у -" + this.y);
    }

}

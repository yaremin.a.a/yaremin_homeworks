import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FunctionWithListAutoImpl implements FunctionWithListAuto {
    List<Auto> autoList;

    @Override
    public List<Auto> findAll(String file) {
        try {
            Stream<String> streamFromFiles = Files.lines(Paths.get(file));
            autoList = streamFromFiles.map(line -> {
                String[] arrayLine = line.split("\\|");
                String number = arrayLine[0];
                String name = arrayLine[1];
                String color = arrayLine[2];
                int mileage = Integer.parseInt(arrayLine[3]);
                int price = Integer.parseInt(arrayLine[4]);
                return new Auto(number, name, color, mileage, price);
            }).collect(Collectors.toList());
        } catch (IOException ignore) {

        }

        return autoList;
    }

    @Override
    public List<String> searchNumber(List<Auto> listAuto, String color) {
        return this.autoList.stream()
                .filter(auto -> auto.getColor().equals(color) || auto.getMileage() == 0)
                .map(Auto::getNumber)
                .collect(Collectors.toList());
    }

    @Override
    public long getCountOfUniqueModels(List<Auto> listAuto, int min, int max) {
        return this.autoList.stream()
                .filter(auto -> auto.getPrice() >= min && auto.getPrice() <= max)
                .distinct()
                .count();
    }

    @Override
    public String getColourAutoWithMinPrice(List<Auto> listAuto) {
        return this.autoList.stream()
                .min(Comparator.comparingInt(Auto::getPrice))
                .get()
                .getColor();
    }

    @Override
    public Double getAveragePrice(List<Auto> listAuto, String model) {
        return this.autoList.stream()
                .filter(auto -> auto.getModel().equals(model))
                .mapToInt(Auto::getPrice)
                .average()
                .getAsDouble();
    }
}

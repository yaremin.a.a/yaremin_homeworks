
import java.util.List;

/**
 * Подготовить файл с записями, имеющими следующую структуру:
 * [НОМЕР_АВТОМОБИЛЯ][МОДЕЛЬ][ЦВЕТ][ПРОБЕГ][СТОИМОСТЬ]
 *
 * o001aa111|Camry|Black|133|82000
 * o002aa111|Camry|Green|133|0
 * o001aa111|Camry|Black|133|82000
 *
 * Используя Java Stream API, вывести:
 *
 * Номера всех автомобилей, имеющих черный цвет или нулевой пробег. // filter + map
 * Количество уникальных моделей в ценовом диапазоне от 700 до 800 тыс. // distinct + filter
 * * Вывести цвет автомобиля с минимальной стоимостью. // min + map
 * * Среднюю стоимость Camry *
 *
 * https://habr.com/ru/company/luxoft/blog/270383/
 *
 * Достаточно сделать один из вариантов.
 */
public class Main {
    public static void main(String[] args) {
        FunctionWithListAuto functionWithListAuto = new FunctionWithListAutoImpl();
        System.out.println("Список автомобилей:");
        List<Auto> autoList = functionWithListAuto.findAll("input.txt");
        autoList.forEach(System.out::println);

        System.out.println("Результат работы функции поиска номеров по цвету:");
        List<String> listNumbers = functionWithListAuto.searchNumber(autoList, "White");
        listNumbers.forEach(System.out::println);

        System.out.println("Результат работы функции подсчета количества автомобилей в заданном диапазоне цены");
        System.out.println(functionWithListAuto.getCountOfUniqueModels(autoList, 600000, 800000));

        System.out.println("Резултат работы функции поиска цвета автомобиля с минимальной стоимостью");
        System.out.println(functionWithListAuto.getColourAutoWithMinPrice(autoList));

        System.out.println("Резултат работы функции вывода средней цены заданной модели:");
        System.out.println(functionWithListAuto.getAveragePrice(autoList, "Camry"));

    }
}

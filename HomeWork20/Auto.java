public class Auto {
    private String number;
    private String model;
    private String colour;
    private int mileage;
    private int price;

    public Auto(String number, String model, String color, int mileage, int price) {
        this.number = number;
        this.model = model;
        this.colour = color;
        this.mileage = mileage;
        this.price = price;
    }

    public String getNumber() {
        return number;
    }

    public String getModel() {
        return model;
    }

    public String getColor() {
        return colour;
    }

    public int getMileage() {
        return mileage;
    }

    public int getPrice() {
        return price;
    }


    @Override
    public String toString() {
        return "Auto{" +
                "number='" + number + '\'' +
                ", model='" + model + '\'' +
                ", colour='" + colour + '\'' +
                ", mileage=" + mileage +
                ", price=" + price +
                '}';
    }
}

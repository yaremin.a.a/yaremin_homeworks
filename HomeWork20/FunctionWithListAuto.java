import java.util.List;

public interface FunctionWithListAuto {
    List<Auto> findAll(String file);
    List<String> searchNumber(List<Auto> listAuto, String color);
    long getCountOfUniqueModels(List<Auto> listAuto, int min, int max);
    String getColourAutoWithMinPrice(List<Auto> listAuto);
    Double getAveragePrice(List<Auto> listAuto, String model);
}

import java.util.Scanner;

/**
 * Реализовать removeAt(int index) для ArrayList
 * Реализовать метод T get(int index) для LinkedList
 */

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        ArrayList<Integer> numbers = new ArrayList<>();
        LinkedList<Integer> numbers2 = new LinkedList<>();
        numbers.add(5);
        numbers.add(7);
        numbers.add(15);
        numbers.add(4);
        numbers.add(32);
        numbers.add(78);
        System.out.println("Ввели список:");
        for (int i = 0; i < numbers.size(); i++) {
            System.out.println(numbers.get(i));
        }
        System.out.println("Введите индекс числа для удаления его из списка");
        numbers.removeAt(scanner.nextInt());
        System.out.println("Список после удаления:");
        for (int i = 0; i < numbers.size(); i++) {
            System.out.println(numbers.get(i));

        }
        numbers2.add(5);
        numbers2.add(7);
        numbers2.add(15);
        numbers2.add(4);
        numbers2.add(32);
        numbers2.add(78);
        System.out.println("Связанный список:");
        for (int i = 0; i < numbers2.size(); i++) {
            System.out.println(numbers2.get(i));
        }
        System.out.println("Введите индекс числа для вывода на экран.");
        System.out.println(numbers2.get(scanner.nextInt()));

    }
}
